import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const configService = new ConfigService();
  const APP_PORT = configService.get<string>('APP_PORT') || 5000;
  const app = await NestFactory.create(AppModule);
  await app.listen(APP_PORT);
}
bootstrap();
